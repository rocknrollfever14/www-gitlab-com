---
layout: markdown_page
title: Product Stage Direction - Defend
---

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Protecting cloud-native applications, services, and infrastructure</b>
    </font>
</p>

GitLab Defend enables organizations to proactively protect their cloud-native environments by providing context-aware
technologies to reduce your overall security risk.  Defend is a natural extension of your existing operations practices
providing security visibility across the entire DevSecOps lifecycle.  This empowers your organization
to apply DevSecOps best practices with visibility from the first line of code written all the way through monitoring
and protecting your applications deployed into operations.

![Defend Overview](/images/direction/defend/defend-overview.png)

## Stage Overview

The Defend Stage focuses on providing security visibility across the entire DevSecOps lifecycle as well as providing
monitoring and mitigation of attacks targeting your cloud-native environment.  Defend reduces overall security risk
by enabling you to be ready now for the cloud-native application stack and DevSecOps best practices of the future.
This is accomplished by:

* providing Security Dashboards enabling your organization to have full visibility from the first line of code written to your application deployed in production
* protecting your applications and services deployed within your cloud-native environment by leveraging context-aware technologies across the entire application stack
* monitoring for misuse within your GitLab deployment&mdash;including its associated runners&mdash;and alerting when anomalies in usage are detected

### Groups

The Defend Stage is made up of two groups supporting the major categories focused on cloud-native security including:

* Threat Insights - Holistically view, manage, and reduce potential risks across the entire DevSecOps lifecycle as well as monitor misuse of your GitLab instance.
* Container Security - Monitor and protect your cloud-native applications and services by leveraging context-aware knowledge to improve your overall security posture.

### Resourcing and Investment

The existing team members for the Defend Stage can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=defend-section)
* [User Experience](https://about.gitlab.com/company/team/?department=defend-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=defend-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)

## Guiding Principles

### Listen and record first, then act
In line with our [Security Paradigm](https://about.gitlab.com/direction/secure/#security-paradigm), Defend features will inform
and report threats that occur as a first step. Once you explicitly tell GitLab to block traffic, users, or devices,
we will record these decisions and then block and drop actions by those bad actors.

This is valuable to you because it means that you can introduce and configure security for your app gradually
over time without disrupting your end users. Examining the information about what GitLab Defend reports in
your app helps you ensure that the security settings are appropriate for your app and business will not
introduce more false positves nor permit more potentially bad actions than you are comfortable with. Having 
recorded evidence of threats and your response ensures you can easily achieve your compliance goals and requirements.

### Inform action for other stages
One of GitLab's key advantages as a single DevOps platform is that all of our stages are integrated and tightly
connected. Defend will identify and protect against threats as they happen, but we will strive to be informative
to other stages to give you actionable next steps to close a vulnerability or point of exploit, not just defend it.

Not only does shifting left and acting on results earlier give your apps better security, it helps
[enable collaboration](/handbook/product/#enabling-collaboration) with everyone at your company.
We believe that security is everyone's responsibility and that [everyone can contribute](/company/strategy/#mission),
and informing other stages is a powerful way to do this. 

### Emphasize usability and convention over configuration
Defend capabilities will be pre-configured to provide value to protecting your applications.
Rather than require you to read documentation manuals and provide complex configuration files,
[GitLab will always provide reasonable defaults](/handbook/product/#convention-over-configuration)
out of the box.

We will provide the ability for advanced and customized configurations, but these will
only be needed based on your specific use case and when you feel comfortable doing so.

<%= partial("direction/categories", :locals => { :stageKey => "defend" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "defend" }) %>
